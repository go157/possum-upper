package main

import (
	"log"

	"gitlab.com/go157/possum-upper/internal/cli"
)

func main() {
	cmd := cli.New()
	err := cmd.Do()
	if err != nil {
		log.Panicln(err)
	}
}
