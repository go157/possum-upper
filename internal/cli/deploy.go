package cli

import (
	"errors"
	"time"

	"gitlab.com/go157/possum-upper/internal/config"
	"gitlab.com/go157/possum-upper/internal/gitlab"
)

type deploy struct {
	gitlabClient gitlab.Client
	projects     []config.Project
}

var ErrJobIsDoneTimeout = errors.New("waiting for job is done timeout")

const (
	waitJobFor           = time.Minute * 5
	askJobResultInterval = time.Minute
)

func (c deploy) Do() error {
	for _, project := range c.projects {
		err := c.deployProject(project)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c deploy) deployProject(project config.Project) error {
	pipelineID, err := c.gitlabClient.GetLastPipelineID(project.ID, project.MasterBranch)
	if err != nil {
		return err
	}

	jobID, err := c.gitlabClient.GetDeployJobID(project.ID, pipelineID)
	if err != nil {
		return err
	}

	retryID, err := c.gitlabClient.RetryJob(project.ID, jobID)
	if err != nil {
		return err
	}

	limit := time.Now().Add(waitJobFor)
	for time.Now().Before(limit) {
		time.Sleep(askJobResultInterval)
		done, err := c.gitlabClient.JobIsDone(project.ID, retryID)
		switch {
		case err != nil:
			return err
		case done:
			return nil
		}
	}

	return ErrJobIsDoneTimeout
}
