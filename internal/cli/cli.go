package cli

import (
	"errors"
	"flag"
	"log"
	"os"

	"gitlab.com/go157/possum-upper/internal/config"
	"gitlab.com/go157/possum-upper/internal/gitlab"
)

type Command interface {
	Do() error
}

type help struct{}

type commandError struct {
	err error
}

var ErrUnknownCommand = errors.New("unknown command")

func New() Command {
	if len(os.Args) == 0 {
		return help{}
	}

	path := flag.String("config", "config.json", "config path")
	conf, err := config.New(*path)
	if err != nil {
		return newError(err)
	}

	switch os.Args[0] {
	case "help":
		return help{}
	case "deploy":
		gitlabClient, gitlabErr := gitlab.New(gitlab.Instance{
			URL:   conf.GitlabURL,
			Token: conf.GitlabToken,
		})
		if gitlabErr != nil {
			return newError(gitlabErr)
		}

		return deploy{
			gitlabClient: gitlabClient,
			projects:     conf.Projects,
		}

	default:
		return newError(ErrUnknownCommand)
	}
}

func newError(message error) commandError {
	return commandError{
		err: message,
	}
}

func (c commandError) Do() error {
	return c.err
}

func (c help) Do() error {
	log.Println(``)

	return nil
}
