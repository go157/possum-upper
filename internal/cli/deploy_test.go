package cli

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/go157/possum-upper/internal/config"
	"gitlab.com/go157/possum-upper/internal/gitlab"
)

func Test_deploy_Do(t *testing.T) {
	t.Parallel()

	type fields struct {
		gitlabClient gitlab.Client
		projects     []config.Project
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "valid case",
			fields: fields{
				gitlabClient: func() gitlab.Client {
					m := gitlab.NewMockClient(gomock.NewController(t))
					m.EXPECT().GetLastPipelineID(
						1,
						"master",
					).Return(
						10,
						nil,
					)
					m.EXPECT().GetDeployJobID(
						1,
						10,
					).Return(
						100,
						nil,
					)
					m.EXPECT().RetryJob(
						1,
						100,
					).Return(
						101,
						nil,
					)
					first := m.EXPECT().JobIsDone(
						1,
						101,
					).Return(
						false,
						nil,
					)
					m.EXPECT().JobIsDone(
						1,
						101,
					).Return(
						true,
						nil,
					).After(first)
					return m
				}(),
				projects: []config.Project{
					{
						ID:           1,
						MasterBranch: "master",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			c := deploy{
				gitlabClient: tt.fields.gitlabClient,
				projects:     tt.fields.projects,
			}
			err := c.Do()
			require.Equal(t, err != nil, tt.wantErr)
		})
	}
}
