package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	GitlabToken string    `json:"gitlab_token"`
	GitlabURL   string    `json:"gitlab_url"`
	Projects    []Project `json:"projects"`
}

type Project struct {
	ID           int    `json:"id"`
	MasterBranch string `json:"master_branch"`
}

func New(path string) (Config, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return Config{}, err
	}

	return parseConfig(content)
}

func parseConfig(data []byte) (Config, error) {
	config := Config{}
	err := json.Unmarshal(data, &config)
	if err != nil {
		return Config{}, err
	}

	return config, nil
}
