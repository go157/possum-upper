package gitlab

import (
	"errors"
	"fmt"

	"github.com/xanzy/go-gitlab"
)

type Instance struct {
	URL   string
	Token string
}

type Client interface {
	GetLastPipelineID(rpojectID int, branch string) (int, error)
	GetDeployJobID(projectID int, pipelineID int) (int, error)
	RetryJob(projectID int, jobID int) (int, error)
	JobIsDone(projectID int, jobID int) (bool, error)
}

type client struct {
	api *gitlab.Client
}

var (
	ErrNoLastPipeline = errors.New("cant find last pipeline")
	ErrNoDeployJob    = errors.New("cant find job deploy")
	ErrJobIsFailed    = errors.New("job has terminate status")
)

func New(desc Instance) (Client, error) {
	git, err := gitlab.NewClient(
		desc.Token,
		gitlab.WithBaseURL(desc.URL),
	)
	if err != nil {
		return nil, err
	}

	return client{api: git}, nil
}

func (c client) GetLastPipelineID(rpojectID int, branch string) (int, error) {
	opt := &gitlab.ListProjectPipelinesOptions{
		Scope:   gitlab.String("branches"),
		Ref:     gitlab.String(branch),
		OrderBy: gitlab.String("updated_at"),
	}

	pipelines, _, err := c.api.Pipelines.ListProjectPipelines(rpojectID, opt)
	switch {
	case err != nil:
		return 0, err
	case len(pipelines) == 0:
		return 0, ErrNoLastPipeline
	}

	return pipelines[0].ID, nil
}

func (c client) GetDeployJobID(projectID int, pipelineID int) (int, error) {
	jobs, _, err := c.api.Jobs.ListPipelineJobs(projectID, pipelineID, nil)
	if err != nil {
		return 0, err
	}

	var jobID int
	for _, job := range jobs {
		if job.Name == "deploy" {
			jobID = job.ID
		}
	}
	if jobID == 0 {
		return 0, ErrNoDeployJob
	}

	return jobID, nil
}

func (c client) RetryJob(projectID int, jobID int) (int, error) {
	job, _, err := c.api.Jobs.RetryJob(projectID, jobID, nil)
	if err != nil {
		return 0, err
	}

	return job.ID, nil
}

func (c client) JobIsDone(projectID int, jobID int) (bool, error) {
	job, _, err := c.api.Jobs.GetJob(projectID, jobID)
	if err != nil {
		return false, err
	}

	switch job.Status {
	case "failed", "cancelled":
		return false, fmt.Errorf("%w: %s", ErrJobIsFailed, job.Status)
	case "success":
		return true, nil
	}

	return false, nil
}
