
init:
	go get -d ./...

lint: init
	golangci-lint run

build: init
	CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -o ./main -a -installsuffix cgo ./cmd/main.go

cover:
	go test -timeout 180s -short -count=1 -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out
	rm coverage.out

test: init
	go test -v -timeout 180s -count=1 ./... -coverprofile=coverage.txt -covermode count
	go get github.com/boumenot/gocover-cobertura
	go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
	go install gotest.tools/gotestsum@latest
	gotestsum --junitfile report.xml --format testname

mockgen:
	mockgen -source=internal/gitlab/gitlab.go -destination=internal/gitlab/gitlab_mock.go -package=gitlab
